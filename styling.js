define(function(){
	if(typeof document.getElementById('p_body') === 'undefined'){
		console.log('The p_body div was not found !');
		return;
	}else{
		var Styling = {
			proportions : {
				height : 0.675*0.85,
				width : 0.367*0.85
			},
			isMobile : function(){
				// return true;
				return window.cordova || Styling.isIOS() || Styling.isAndroid();
			},

			isIOS : function(){
				// return true;
				if(window.device && window.device.platform && window.device.platform.toLowerCase() === 'ios') return true;
				else if(/iPhone|iPad|iPod/i.test(navigator.userAgent)) return true;
				else return false;
			},
			isAndroid : function(){
				if(window.device && window.device.platform && window.device.platform.toLowerCase() === 'android') return true;
				else if(/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) return true;
				else return false;
			},

			getHeight : function(element){
				if(!element) element = document;
				if(Styling.isMobile() && element === document){
					return Styling.getScreenHeight();
				}else if(element === document){
					return Styling.proportions.height*screen.height;
				}else{
					return Math.max(
						element.scrollHeight,
						element.offsetHeight,
						element.clientHeight
					);
				}
			},
			getWidth : function(element){
				if(!element) element = document;
				if(Styling.isMobile() && element === document){
					return Styling.getScreenWidth();
				}else if(element === document){
					return Styling.proportions.width*screen.height;
				}else{
					return Math.max(
						element.scrollWidth,
						element.offsetWidth,
						element.clientWidth
					);
				}
			},
			maxWidth : 1500,
			getSizeCategory : function(){
					 if(Styling.getWidth() > Styling.maxWidth * 0.7) return 'lg';
				else if(Styling.getWidth() > Styling.maxWidth * 0.5) return 'md';
				else 												 return 'sm';
			},

			getScreenHeight : function(){
				if(!Styling.screenHeight){
					Styling.screenHeight = Math.max(
						document.body.scrollHeight, document.documentElement.scrollHeight,
						document.body.offsetHeight, document.documentElement.offsetHeight,
						document.body.clientHeight, document.documentElement.clientHeight
					);
				}

				return Styling.screenHeight;
			},
			getScreenWidth : function(){
				if(!Styling.screenWidth){
					Styling.screenWidth = Math.max(
						document.body.scrollWidth, document.documentElement.scrollWidth,
						document.body.offsetWidth, document.documentElement.offsetWidth,
						document.body.clientWidth, document.documentElement.clientWidth
					);
				}

				return Styling.screenWidth;
			},

			getPositionInPage : function(element, container){
				if(!container) container = document.getElementById('p_body');
				var containerRect 	= container.getBoundingClientRect();
				var elemRect 		= element.getBoundingClientRect();

				var position = {};
				for(var key in elemRect) position[key] = elemRect[key];
					position.top 	-= containerRect.top;
					position.bottom -= containerRect.top;
					position.left 	-= containerRect.left;
					position.right 	-= containerRect.left;

				return position;
			},

			addClass : function(object, className){
				if(!object){
					console.log(className);
					var err = new Error();
					console.log(err.stack);
					return object;
				}
				
				if(object.className.indexOf(className) === -1) object.className += ' ' + className;
				return object;
			},
			removeClass : function(object, className){
				if(!object){
					console.log(className);
					return object;
				}

				var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
				object.className = object.className.replace(reg, ' ');

				return object;
			},
			toggleClass : function(object, className){
				if(!object) return false;

				var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
				if(object.className.indexOf(className) === -1) object.className += ' ' + className;
				else object.className = object.className.replace(reg, ' ');
			},
			hasClass : function(object, className){
				if(!object) return false;

				var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
				return reg.test(object.className);
			},
			hasParent : function(object, className, forbiddenClassNames){
				if(!forbiddenClassNames) forbiddenClassNames = [];

				var parent = object;
				while(parent && !Styling.hasClass(parent, className)){
					for(var i = 0; i < forbiddenClassNames.length; i++){
						if(Styling.hasClass(parent, forbiddenClassNames[i])) return false;
					}
					parent = parent.parentNode;
				}
				if(!parent) return false;

				return true;
			},
			getParent : function(object, className, forbiddenClassNames){
				if(!forbiddenClassNames) forbiddenClassNames = [];

				var parent = object;
				while(parent && !Styling.hasClass(parent, className)){
					for(var i = 0; i < forbiddenClassNames.length; i++){
						if(Styling.hasClass(parent, forbiddenClassNames[i])) return null;
					}
					parent = parent.parentNode;
				}
				if(!parent) console.log('parent object not found', className, object, forbiddenClassNames);
				return parent;
			},
			getBaseFontsize : function(){
				return Math.min(Styling.getWidth() * 0.055, 20);
			}
		};

		// GAME SIZE
			var pBody = document.getElementById('p_body');
				pBody.style.height 		= Styling.getHeight() + 'px';
				pBody.style.width 		= Styling.getWidth() + 'px';
				pBody.style.fontSize 	= Styling.getBaseFontsize() + 'px';

			console.log('Base Fontsize : ' + Styling.getBaseFontsize());

			var pageContainer = document.getElementById('page-container');
			if(Styling.isMobile()){
				Styling.addClass(pageContainer, 'mobile');
				Styling.removeClass(pageContainer, 'web');
			}else{
				var mobileBackground = document.getElementById('img_background');
					mobileBackground.style.height = Styling.getHeight() * 690 / 540 + 'px';

				Styling.addClass(pageContainer, 'web');
				Styling.removeClass(pageContainer, 'mobile');
			}

		return Styling;
	}
});