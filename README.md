# styling #

v1.0.47

This module defines basic styling methods such as getWidth and getHeight. It also defines the device size category.

It contains 3 methods :

- ** getWidth ** : returns the width of an element in pixels.
- ** getHeight ** : returns the height of an element in pixels.
- ** getPositionInWindow ** : returns the position of an element relatively to its first parent.

It contains the size category attribute ** deviceSize **.

** For this code to work **, you need to put all of your app inside a div with id='p_body'.
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1, width=device-width, height=device-height, minimal-ui"/>
        <title>Example</title>
        <link rel="stylesheet" type="text/css" href="../[...]/Styling.css" />
    </head>
    <body>
        <!-- PAGES -->
        <div id="page-container">
            <div id="p_body">
                <!-- GAME PAGE -->
                <div class="Page flashingPage flashingGroup1" id="flashingPage11">
                    <header class="title-header">
                        <h1>Title</h1>
                    </header>
                    <div class="PageBody">
                 		[...]
                    </div>
                    <footer>
                        <p> Advertising </p>
                    </footer>
                </div>

                <!-- CACHE PAGE -->
                <div id="cacheBlock">
                    <div id="ajaxLoading">
                        <img alt="loadingImg" />
                    </div>
                </div>
            </div>
        </div>

        <!-- ADVERTISING -->
        <aside id="adSense">[...]</aside>
        
        <!-- SCRIPTS -->
        <script type="text/javascript" src="js/index.js"></script>
    </body>
</html>
```

## getWidth and getHeight ##
```js
getWidth([element]);
getHeight([element]);
```

- ** element ** : optional DOM element. *Default value is document*.

#### Example ####
```js
var Styling = require('Styling');

var myDiv = document.getElementById('myDiv');
myDiv.style.width = '300px';

Styling.getWidth(myDiv); // returns 300


Styling.getHeight() // return the document's height.
```

** /!\ On desktop /!\ **

- the document height is fixed to 80% of the screen height.
- the document width is fixed to 60% of the screen height.

## deviceSize ##

deviceSize is the size category of the viewport. It is very useful to load images with the right resolution.

- 0 => 0px < device_width < 1000px
- 1 => 1000px < device_width

## getPositionInWindow ##
```js
getPositionInWindow(element);
```

- ** element ** : DOM element.

This function returns the element coordinates in its parent in pixels as a JSON object : 
```json
{
	dx : 120, 
	dy : 200
}
```